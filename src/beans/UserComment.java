package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String text;
	private Date createdDate;
	private int userId;
	private String userName;
	private int messageId;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
