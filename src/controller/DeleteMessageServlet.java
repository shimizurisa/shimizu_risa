package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DeleteContentsService;

@WebServlet(urlPatterns = "/messageDelete")
public class DeleteMessageServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		int messageId = Integer.parseInt(request.getParameter("message_id"));

		DeleteContentsService deleteContentsService = new DeleteContentsService();

		deleteContentsService.deleteMessage(messageId);

		response.sendRedirect("./");


	}
}
