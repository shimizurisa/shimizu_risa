package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches =new BranchService().getBranch();

		List<Department> departments =new DepartmentService().getDepartment();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("SignUp.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
		user.setIs_deleted(0);

		if (isValid(request, messages) == true) {

			new UserService().register(user);

			response.sendRedirect("management");

		} else {

			List<Branch> branches =new BranchService().getBranch();
			List<Department> departments =new DepartmentService().getDepartment();

			request.setAttribute("signupUser", user);
			session.setAttribute("errorMessages", messages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("SignUp.jsp").forward(request, response);

		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordConfirm = request.getParameter("password-confirm");
		int branchId = Integer.parseInt(request.getParameter("branch_id"));
		int departmentId = Integer.parseInt(request.getParameter("department_id"));

		User user = new UserService().getUserId(account);
//		String userAccount = user.getAccount();


		//ログインIDが空白のときエラー
		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");

		//ログインIDが半角英数字6-20文字でなかったときエラー
		}else if (!account.matches("^[0-9a-zA-Z]{6,20}")){
			messages.add("ログインIDは半角英数字6～20文字で入力してください");

		}else if(user != null) {
			messages.add("既に使われているログインIDです");
		}

		//パスワードが空白のときエラー
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");

		//ログインIDが記号を含む半角英数字6-20文字でなかったときエラー
		}else if (!password.matches("^[0-9a-zA-Z\\p{Punct}]{6,20}")){
				messages.add("パスワードは記号を含む半角英数字6～20文字で入力してください");

		//パスワード(登録用)と(確認用)が一致しないときエラー
		}else if (!password.equals(passwordConfirm)) {

			messages.add("パスワード(登録用)とパスワード(確認用)が一致していません");
		}
		//名称が空白のときエラー
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");

		}else if (name.length() >= 11){
			messages.add("名前は10文字以下で入力してください");

		}

		if(branchId == 1 && departmentId == 3){
			messages.add("支店名と部署・役職の組み合わせが不正です");
		}
		if(branchId >= 2 && departmentId <= 2){
			messages.add("支店名と部署・役職の組み合わせが不正です");
		}



		if (messages.size() == 0) {
			return true;

		} else {
			return false;
		}
	}

}


