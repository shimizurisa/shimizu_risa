package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserComment;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//入力されたデータを受け取る
		HttpSession session = request.getSession();


		List<String> comments = new ArrayList<String>();

		//sessionに入っているログインユーザー情報をUser型にキャストする。
		User user = (User) session.getAttribute("loginUser");

		//Beanに入れる
		UserComment comment = new UserComment();

		comment.setText(request.getParameter("new-comment"));
		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("message-id")));

		if (isValid(request, comments) == true){

			new CommentService().register(comment);

			//つぎのページの表示
			response.sendRedirect("./");
;

		}else {

			//エラーメッセージエリアに名前をつける。
			//変数を投稿のidに指定する。
			session.setAttribute("errorMessages", comments);
			session.setAttribute("MessageID", request.getParameter("message-id"));
			session.setAttribute("inputComment", comment);


			response.sendRedirect("./?#commen-area-" + request.getParameter("message-id"));
		}
	}



	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("new-comment");

		if (StringUtils.isBlank(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
