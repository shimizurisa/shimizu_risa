package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> users =new UserService().getAllUsers();
		request.setAttribute("users", users);

		request.getRequestDispatcher("userManagement.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		int userIsDeleted = Integer.parseInt(request.getParameter("user_is_deleted"));
		int userId = Integer.parseInt(request.getParameter("user_id"));

		UserService userService = new UserService();

		userService.isDeleted(userIsDeleted, userId);


		response.sendRedirect("management");


	}

}