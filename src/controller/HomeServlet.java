package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//カテゴリー検索機能
		String searchCategory = request.getParameter("search-category");


		//日付検索機能
		//入力された日付を取得
		String from = request.getParameter("from");
		String to = request.getParameter("to");

		//入力が空のとき
		if (StringUtils.isEmpty(from) == true){

			from = "2018-04-01 00:00:00";

		//入力された値+ 0:00:00を作る
		}else {
			request.setAttribute("from", from);
			from = from + " 0:00:00";
		}

		//入力が空のとき
		if (StringUtils.isEmpty(to) == true){

			Date date = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			to = sdf1.format(date);


		}else {
			request.setAttribute("to", to);
			to = to + " 23:59:59";
		}

		List<UserMessage> messages =new MessageService().getUserMessage(searchCategory, from, to);
		request.setAttribute("messages", messages);

		List<UserComment> comments =new CommentService().getUserComment();
		request.setAttribute("comments", comments);
		//カテゴリー検索の値の保持
		request.setAttribute("category", searchCategory);

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}
}
