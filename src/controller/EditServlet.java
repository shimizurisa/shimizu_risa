package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = "/edit")
public class EditServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		//セッションよりログインユーザーの情報を取得

		String user = request.getParameter("userId");

		if (StringUtils.isEmpty(user) || !user.matches("\\d{1,}")){

			messages.add("不正なパラメーターです。");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("management");
			return;
		}

		int userId = Integer.parseInt(user);

		//編集するユーザー情報のidを元にDBからユーザー情報取得
		User editUser = new UserService().getUser(userId);

		if(editUser == null){

			messages.add("不正なパラメーターです。");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("management");
			return;
		}

		List<Branch> branches =new BranchService().getBranch();

		List<Department> departments =new DepartmentService().getDepartment();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("userEdit.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User editUser = getEditUser(request);


		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("userEdit.jsp").forward(request, response);
				return;
			}


			response.sendRedirect("./management");

		} else {
			List<Branch> branches =new BranchService().getBranch();
			List<Department> departments =new DepartmentService().getDepartment();


			session.setAttribute("errorMessages", messages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("userEdit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

		return editUser;

	}


	private boolean isValid(HttpServletRequest request, List<String> messages)
			throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordConfirm = request.getParameter("password-confirm");
		int branchId = Integer.parseInt(request.getParameter("branch_id"));
		int departmentId = Integer.parseInt(request.getParameter("department_id"));

		User userId = new UserService().getUserId(account);
		User editUser = getEditUser(request);


		//ログインIDが空白のときエラー
		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");

		//ログインIDが半角英数字6-20文字でなかったときエラー
		}else if (!account.matches("^[0-9a-zA-Z]{6,20}")){
			messages.add("ログインIDは半角英数字6～20文字で入力してください");

		}else if(userId != null && userId.getId() != editUser.getId()) {
			messages.add("既に使われているログインIDです");
		}


		//パスワードが空白のときエラー
		if (!StringUtils.isEmpty(password) == true) {
			//パスワードが記号を含む半角英数字6-20文字でなかったときエラー
			if (!password.matches("^[0-9a-zA-Z\\p{Punct}]{6,20}")){
				messages.add("パスワードは記号を含む半角英数字6～20文字で入力してください");

			//パスワード(登録用)と(確認用)が一致しないときエラー
			}else if (!password.equals(passwordConfirm)) {

				messages.add("パスワードとパスワード(確認用)が一致していません");
			}
		}
		//名称が空白のときエラー
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");

		}else if (name.length() >= 11){
			messages.add("名前は10文字以下で入力してください");

		}

		if(branchId == 1 && departmentId == 3){
			messages.add("支店名と部署・役職の組み合わせが不正です");
		}
		if(branchId >= 2 && departmentId <= 2){
			messages.add("支店名と部署・役職の組み合わせが不正です");
		}



		if (messages.size() == 0) {
			return true;

		} else {
			return false;
		}
	}
}
