package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {



	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy(){
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String servletPath = ((HttpServletRequest)request).getServletPath();

		System.out.println(servletPath);

		if(!servletPath.equals("/login") && !servletPath.endsWith(".css")){

			HttpSession session = ((HttpServletRequest)request).getSession();

			if(session.getAttribute("loginUser") != null){
				chain.doFilter(request, response);
				return;

			}else {

				String message = "ログインしてください";
				session.setAttribute("errorMessages", message);

				// セッションがNullならば、ログイン画面へ飛ばす
				((HttpServletResponse)response).sendRedirect("./login");
				return;
			}
		}
		chain.doFilter(request, response);
	}

}
