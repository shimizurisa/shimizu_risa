package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/management", "/signup", "/edit"})
public class ManagementAccessFilter implements Filter{

	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy(){
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {


		HttpSession session = ((HttpServletRequest)request).getSession();

		User user = (User) session.getAttribute("loginUser");


		if (user.getDepartment_id() == 1){

			chain.doFilter(request, response);

		}else {

			String message = "アクセス権限がありません";
			session.setAttribute("filterMessages", message);


			// セッションがNullならば、ホーム画面へ飛ばす
			((HttpServletResponse)response).sendRedirect("./");

//			RequestDispatcher dispatcher = request.getRequestDispatcher("./");
//			dispatcher.forward(request,response);
		}
	}
}

