package service;

import java.sql.Connection;
import java.util.List;

import beans.UserMessage;
import dao.MessageDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class MessageService {

	public void register(UserMessage message){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			DBUtil.commit(connection);
		}catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e){
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}

	public List<UserMessage> getUserMessage(String searchCategory, String from, String to){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			MessageDao messageDao = new MessageDao();
			List<UserMessage> ret = messageDao.getMessages(connection, searchCategory, from, to);

			DBUtil.commit(connection);

			return ret;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}
}
