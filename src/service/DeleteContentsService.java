package service;

import java.sql.Connection;

import dao.DeleteContentsDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class DeleteContentsService {

	public void deleteComment(int commentId){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			DeleteContentsDao deleteContentsDao = new DeleteContentsDao();

			deleteContentsDao.deleteComment(connection, commentId);

			DBUtil.commit(connection);

		}catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e){
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}

	public void deleteMessage(int messageId){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			DeleteContentsDao deleteContentsDao = new DeleteContentsDao();

			deleteContentsDao.deleteMessage(connection, messageId);

			DBUtil.commit(connection);

		}catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e){
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}

}
