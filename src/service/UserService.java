package service;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;

public class UserService {

	public void register(User user){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			DBUtil.commit(connection);

		} catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally {
			CloseableUtil.close(connection);

		}
	}

	public List<User> getAllUsers(){
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			List<User> ret = userDao.getUsers(connection);

			DBUtil.commit(connection);

			return ret;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}

	public User getUser(int userId) {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getEditUser(connection, userId);

			DBUtil.commit(connection);

			return user;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;

		}catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);

		}
	}

	public void update(User user) {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			if (!StringUtils.isBlank(user.getPassword())){

				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);

			}

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			DBUtil.commit(connection);

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e) {
				DBUtil.rollback(connection);
				throw e;

		}finally {

			CloseableUtil.close(connection);
		}
	}

	public void isDeleted(int userIsDeleted, int userId) {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();


			UserDao userDao = new UserDao();
			userDao.isDeleted(connection, userIsDeleted, userId);

			DBUtil.commit(connection);

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {

			CloseableUtil.close(connection);
		}
	}

	public User getUserId(String account) {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			User userId = userDao.geUserId(connection, account);

			DBUtil.commit(connection);

			return userId;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;

		}catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);

		}
	}




}
