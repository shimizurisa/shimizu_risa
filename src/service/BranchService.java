package service;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class BranchService {

	public List<Branch> getBranch() {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> branch = branchDao.getBranch(connection);

			DBUtil.commit(connection);

			return branch;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;

		}catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);

		}
	}

}
