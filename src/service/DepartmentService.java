package service;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class DepartmentService {

	public List<Department> getDepartment() {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			DepartmentDao departmentDao = new DepartmentDao();
			List<Department> department = departmentDao.getDepartment(connection);

			DBUtil.commit(connection);

			return department;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;

		}catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);

		}
	}


}
