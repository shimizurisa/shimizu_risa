package service;

import java.sql.Connection;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;

public class LoginService {

	public User login(String account, String password){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, account, encPassword);

			DBUtil.commit(connection);

			return user;

		}catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally {
			CloseableUtil.close(connection);
		}
	}

}
