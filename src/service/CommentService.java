package service;

import java.sql.Connection;
import java.util.List;

import beans.UserComment;
import dao.CommentDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class CommentService {
	public void register(UserComment comment){

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			DBUtil.commit(connection);
		}catch (RuntimeException e){
			DBUtil.rollback(connection);
			throw e;

		}catch (Error e){
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}
	//最新の1000件を持ってきてる
	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getUserComment(){
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			CommentDao commentDao = new CommentDao();
			List<UserComment> ret = commentDao.getComments(connection, LIMIT_NUM );

			DBUtil.commit(connection);

			return ret;

		}catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		}finally {
			CloseableUtil.close(connection);
		}
	}

}
