package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class UserDao {

	public void insert(Connection connection, User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_deleted");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // department_id
			sql.append(", ?"); //is_deleted
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getDepartment_id());
			ps.setInt(6, user.getIs_deleted());

			System.out.println(ps.toString());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	public User getUser(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			//SQL文でアカウントとパスワードが一致するデータを参照している
			String sql = "SELECT * FROM users WHERE account = ? AND password = ? AND is_deleted = 0";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);

			//命令文実行してリストに入れる
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	public User getEditUser(Connection connection, int id) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;

			}else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");

			}else {

				return userList.get(0);
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			CloseableUtil.close(ps);

		}
	}

	//DBからもらってきたデータをリストに整理する(DBの形では扱いにくい)
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//各項目を変数に入れる
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId= rs.getInt("department_id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int is_deleted = rs.getInt("is_deleted");

				//変数に入れたものをbeanに入れる
				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branchId);
				user.setDepartment_id(departmentId);
				user.setCreatedDate(createdDate);
				user.setIs_deleted(is_deleted);

				//リストに入れる
				ret.add(user);
			}
			return ret;
		} finally {
			CloseableUtil.close(rs);
		}
	}

	public List<User> getUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();



			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("branches.name as branch_name, ");
			sql.append("departments.name as department_name, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.is_deleted as is_deleted ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY branch_id ASC, department_id ASC, id ASC");

			ps = connection.prepareStatement(sql.toString());

			System.out.println(sql.toString());

			//命令文実行してリストに入れる
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUsersList(rs);

			return userList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	private List<User> toUsersList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//各項目を変数に入れる
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String branchId = rs.getString("branch_name");
				String departmentId= rs.getString("department_name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int is_deleted = rs.getInt("is_deleted");

				//変数に入れたものをbeanに入れる
				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setCreatedDate(createdDate);
				user.setIs_deleted(is_deleted);

				//リストに入れる
				ret.add(user);
			}
			return ret;
		} finally {
			CloseableUtil.close(rs);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");

			if (user.getPassword() != null){
				if(!StringUtils.isBlank(user.getPassword())){
					sql.append(", password = ?");
				}
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch_id());
			ps.setInt(4, user.getDepartment_id());

			if (user.getPassword() != null && !StringUtils.isBlank(user.getPassword())){
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());

			}else {
				ps.setInt(5, user.getId());

			}

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			CloseableUtil.close(ps);

		}
	}

	public void isDeleted(Connection connection, int userIsDeleted, int userId) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userIsDeleted);
			ps.setInt(2, userId);

			int count = ps.executeUpdate();

				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();

				}

			}catch (SQLException e) {
				throw new SQLRuntimeException(e);

			}finally {
				CloseableUtil.close(ps);

		}
	}

	public User geUserId(Connection connection, String account) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;

			}else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");

			}else {

				return userList.get(0);
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			CloseableUtil.close(ps);

		}
	}

}


