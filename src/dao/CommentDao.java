package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class CommentDao {
	public void insert (Connection connection, UserComment comment){

		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", created_date");
			sql.append(", user_id");
			sql.append(", message_id");
			sql.append(") VALUES (");
			sql.append("?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", ?"); // user_id
			sql.append(", ?"); // message_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getMessageId());


			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}
	public List<UserComment> getComments(Connection connection, int num){

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("users.name as name, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.message_id as message_id ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());



			ResultSet rs = ps.executeQuery();

			List<UserComment> ret = toUserCommentList(rs);

			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			CloseableUtil.close(ps);

		}
	}

	public List<UserComment> toUserCommentList(ResultSet rs) throws SQLException{

		List<UserComment> ret = new ArrayList<UserComment>();

		try {
			while(rs.next()){

				int id = rs.getInt("id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				String name = rs.getString("name");
				int userId = rs.getInt("user_id");
				int messageId = rs.getInt("message_id");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setText(text);
				comment.setUserName(name);
				comment.setCreatedDate(createdDate);
				comment.setUserId(userId);
				comment.setMessageId(messageId);

				ret.add(comment);
			}

			return ret;
		}finally {
			CloseableUtil.close(rs);
		}
	}

}
