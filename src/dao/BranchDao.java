package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class BranchDao {
	public List<Branch> getBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
			//SQL文でアカウントとパスワードが一致するデータを参照している
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql);

			//命令文実行してリストに入れる
			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);

			if (branchList.isEmpty() == true) {
				return null;

			} else {
				return branchList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				//各項目を変数に入れる
				int id = rs.getInt("id");
				String name = rs.getString("name");


				//変数に入れたものをbeanに入れる
				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				//リストに入れる
				ret.add(branch);
			}
			return ret;
		} finally {
			CloseableUtil.close(rs);
		}
	}


}
