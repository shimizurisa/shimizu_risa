package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class DeleteContentsDao {

	public void deleteComment(Connection connection, int commentId){

		PreparedStatement ps = null;

		try{
			String sql = "DELETE FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);

			ps.executeUpdate();



		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);
		}
	}

	public void deleteMessage(Connection connection, int messageId){

		PreparedStatement ps = null;

		try{
			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, messageId);

			ps.executeUpdate();

			System.out.println(ps);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);
		}
	}


}
