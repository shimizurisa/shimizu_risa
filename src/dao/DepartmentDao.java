package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class DepartmentDao {

	public List<Department> getDepartment(Connection connection) {

		PreparedStatement ps = null;
		try {
			//SQL文でアカウントとパスワードが一致するデータを参照している
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			//命令文実行してリストに入れる
			ResultSet rs = ps.executeQuery();
			List<Department> departmentList = toDepartmentList(rs);

			if (departmentList.isEmpty() == true) {
				return null;

			} else {
				return departmentList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while (rs.next()) {
				//各項目を変数に入れる
				int id = rs.getInt("id");
				String name = rs.getString("name");


				//変数に入れたものをbeanに入れる
				Department department = new Department();
				department.setId(id);
				department.setName(name);

				//リストに入れる
				ret.add(department);
			}
			return ret;
		} finally {
			CloseableUtil.close(rs);
		}
	}


}
