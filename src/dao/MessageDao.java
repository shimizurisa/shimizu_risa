package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class MessageDao {

	public void insert (Connection connection, UserMessage message){

		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", user_id");
			sql.append(") VALUES (");
			sql.append("?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", ?"); // user_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());



			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

	public List<UserMessage> getMessages(Connection connection, String searchCategory, String from, String to){

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date, ");
			sql.append("messages.user_id as user_id ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id  ");
			sql.append("where messages.created_date >= ? and messages.created_date <= ? ");

			if (StringUtils.isEmpty(searchCategory) != true){
				sql.append("and category like ? ");
			}

			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, from);
			ps.setString(2, to);

			if (StringUtils.isEmpty(searchCategory) != true){
				ps.setString(3, "%" + searchCategory + "%");
			}

			System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery();

			List<UserMessage> ret = toUserMessageList(rs);

			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			CloseableUtil.close(ps);

		}
	}

	public List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException{

		List<UserMessage> ret = new ArrayList<UserMessage>();

		try {
			while(rs.next()){

				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String name = rs.getString("name");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int userId = rs.getInt("user_id");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setUserName(name);
				message.setCreatedDate(createdDate);
				message.setUserId(userId);

				ret.add(message);


			}

			return ret;
		}finally {
			CloseableUtil.close(rs);
		}
	}
}
