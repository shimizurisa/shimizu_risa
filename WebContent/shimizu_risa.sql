drop table users;
Create database shimizu_risa;
use shimizu_risa;
create table users (
id integer auto_increment Primary key,
account varchar(20) unique not null,
password varchar(255) not null,
name varchar(10),
branch_id integer,
department_id integer,
is_deleted tinyint(1),
created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP 
);

create table branches(
id integer auto_increment Primary key,
name varchar(10) not null
);

create table departments(
id integer auto_increment Primary key,
name varchar(10) not null
);

create table messages(
id integer auto_increment Primary key,
title varchar(30) not null,
text     varchar(1000) NOT NULL,
category varchar(10),
created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ,
user_id integer);

create table comments(
id integer auto_increment Primary key,
text varchar(500),
created_date timestamp not null default current_timestamp,
user_id integer,
message_id integer
);
