<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集</title>
		<link href="./css/signup.css" rel="stylesheet" type="text/css">
	</head>
	<body>

	<div class = "header-fixed">
			<p class = "menu"><a href="management" class = "link_btn"><span>ユーザー管理</span></a>

			<p class ="main-title">ユーザー編集</p>
			<p class = "logout"><a href = "logout"class = "link_btn"><span>ログアウトする</span></a></p>

		</div>

		<div class="main-contents">
		<div class = "form">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
 							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
 				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="edit" method="post">
				<input name="id" value="${editUser.id}" id="id" type="hidden"/>
			<table>
			<tr>
				<td><label for="account">ログインID<br/>(半角英数字6～20文字)</label></td>
				<td><input name="account" value="${editUser.account}" /></td>
			</tr>
			<tr>
				<td><label for="password">パスワード<br/>(記号を含む半角英数字6～20文字)</label></td>
				<td><input name="password" type="password" id="password"/><td/>
			</tr>
			<tr>
				<td><label for="password-confirmm">パスワード(確認用)</label></td>
				<td><input name="password-confirm" type="password" id="password-confirm"/></td>
			</tr>
			<tr>
				<td><label for="name">名前(10文字以下)</label></td>

				<td><input name="name" value = "${editUser.name}" id="name"/></td>
			</tr>

			<tr>
				<td><label for="branch_name">支店</label></td>
					<c:if test = "${editUser.id == loginUser.id}">
						<input type="hidden" name = "branch_id" value="1">
						<td><c:out value = "本社"/></td>

					</c:if>

					<c:if test = "${editUser.id != loginUser.id}">
					<td><select name = "branch_id" id = "branch_id">
							<c:forEach items = "${branches}" var = "branch">
								<c:if test = "${editUser.branch_id == branch.id }">
									<option value = "${branch.id}" selected>${branch.name}</option>
								</c:if>
								<c:if test = "${editUser.branch_id != branch.id }">
									<option value = "${branch.id}">${branch.name}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</c:if>
			</tr>
			<tr>
				<td><label for="department_name">部署・役職</label></td>

					<c:if test = "${editUser.id == loginUser.id}">
						<input type="hidden" name = "department_id" value="1">
						<td><c:out value = "人事総務部"/></td>
					</c:if>

					<c:if test = "${editUser.id != loginUser.id}">

					<td><select name = "department_id" id = "department_id" >
							<c:forEach items = "${departments}" var = "department">

								<c:if test = "${editUser.department_id == department.id }">
									<option value = "${department.id}" selected>${department.name}</option>
								</c:if>
								<c:if test = "${editUser.department_id != department.id }">
									<option value = "${department.id}">${department.name}</option>
								</c:if>

							</c:forEach>
						</select></td>
					</c:if>

			</table>

				<input id = "submit_btn" type="submit" value="更新" /> <br />
			</form>
			</div>


		</div>
		<div class = "copyright">Copyright(c)Shimizu Risa</div>

	</body>
</html>