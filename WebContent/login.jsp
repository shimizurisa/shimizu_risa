<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./css/login.css" rel="stylesheet" type="text/css">
</head>
	<body>
	<div class = "overall">

		<div class = "header-fixed">
			<h1>社内掲示板</h1>
		</div>
		<div class = "main-contents">
			<div class = "form">

			<div class = "erorrMessages">
				<c:if test="${ not empty errorMessages }">

						<ul>
							<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>
			</div>




			<div class = "login-form">
			<form action = "login" method = "post">

				<p class ="a"><label for = "account">ログインID</label></p>
				<p class ="b"><input name = "account" id = "account" value = "${loginID}"></p>

				<p class ="a" ><label for = "password">パスワード</label></p>
				<p class = "b"><input name = "password" type = "password" id = "password"/></p>

				<input id = "submit_btn" type = "submit" value = "ログイン"/>

			</form>
			</div>
			</div>
		</div>

		<div class = "copyright">Copyright(c)Shimizu Risa</div>
	</div>


	</body>
</html>