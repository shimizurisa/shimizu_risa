<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/management.css" rel="stylesheet" type="text/css">

		<script>
		function disp1(){

			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
			if(window.confirm("アカウントを停止しますか？")){

				window.alert('アカウントを停止しました。');
				return true;
		}
			// 「キャンセル」時の処理開始
			else{

				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false;

			}
			// 「キャンセル」時の処理終了

		}

		function disp2(){

			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
			if(window.confirm("アカウントを活動させますか？")){

				window.alert('アカウントを活動中にしました。');
				return true;
			}

			// 「キャンセル」時の処理開始
			else{

				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false;

			}
			// 「キャンセル」時の処理終了

		}



		</script>
	</head>
	<body>

	<div class = "header-fixed">
			<p class = "menu"><a href="signup" class = "link_btn"><span>ユーザー新規登録</span></a>
			<a href="./" class = "link_btn"><span>ホーム</span></a></p>

			<p class ="main-title">ユーザー管理</p>
			<p class = "logout"><a href = "logout"class = "link_btn"><span>ログアウトする</span></a></p>

		</div>



		<div class = "main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
 						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
 			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<table class = "user-table">
			<tr>
				<%--<th>ID</th> --%>
				<th>ログインID</th>
				<th>名前</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>ユーザー</th>
				<th>編集</th>

			</tr>

		<c:forEach items="${users}" var="user">


			<tr>
				<%--<div class="id"><c:out value="${user.id}" /></div>--%>
				<td><c:out value="${user.account}" /></td>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.branchId}" /></td>
				<td><c:out value="${user.departmentId}" /></td>

				<td>
				<c:if test = "${user.id == loginUser.id}">
					<input id = "login" type = "button" value ="ログイン中" disabled>
				</c:if>
				<c:if test = "${user.id != loginUser.id}">
					<c:if test = "${user.is_deleted == 0}">
						<form action = "management" method = "post" >
							<input id = "reset_btn" type = "submit" onClick = "return disp1();" value = "活動中" />
							<input type="hidden" name = "user_is_deleted" value="1">
							<input type="hidden" name = "user_id" value="${user.id}">
						</form>
					</c:if>

					<c:if test = "${user.is_deleted == 1}">
						<form action = "management" method = "post">
							<input id = "submit_btn" type = "submit" onClick = "return disp2();" value = "停止中"/>
							<input type="hidden" name = "user_is_deleted" value="0">
							<input type="hidden" name = "user_id" value="${user.id}">
						</form>
					</c:if>
				</c:if>
				</td>
				<td>
				<form action = "edit" method = "get">
					<input id = "submit_btn" type = "submit" value = "編集">
					<input type = "hidden" value = "${user.id}" name = "userId">
				</form>
				</td>
			</tr>
		</c:forEach>
		</table>
		</div>


			<div class = "copyright">Copyright(c)Shimizu Risa</div>

	</body>
</html>