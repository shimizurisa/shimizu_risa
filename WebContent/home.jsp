<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/home.css" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-3.3.1..js"></script>

	<script>
		//投稿削除の確認ダイアログ


		function resetTest(){

			document.getElementById("search-category").value="";


			document.getElementById("from").value="";
			document.getElementById("to").value="";

		}

		function menu_test(){
			document.getElementById("nav-input").checked=true;
		}
	</script>


	</head>


	<body>
	<div class = "header-fixed">
		<p class = "menu"><a href = "newMessage" class = "link_btn"><span>新規投稿</span></a>

		<c:if test = "${loginUser.department_id == 1}">
			<a href="management" class = "link_btn"><span>ユーザー管理</span></a>
		</c:if></p>


		<p class ="main-title">社内掲示板</p>
		<div class = "logout">


		<a href = "logout"class = "link_btn"><span>ログアウトする</span></a>
		</div>

		<div id="nav-drawer">
			<input id="nav-input" type="checkbox" class="nav-unshown">
			<img src="./img/icon.png" onclick="menu_test();"/>
			<label id="nav-open" for="nav-input"><span></span></label>
			<label class="nav-unshown" id="nav-close" for="nav-input"></label>
      		<div id="nav-content">
      			<form action = "./" method = "get">

      				<h3>検索</h3>
					<div class = "search-area">
						<table>
						<tr>
							<td><label for="search-category">カテゴリー</label></td>
							<td><input name="search-category" id ="search-category" value = "${category}"></td>
						</tr>
						<tr>
							<td><label for = "search-date">日付</label></td>
							<td><input type="date" name="from" id = "from" value = "${from}">～</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="date" name="to" id = "to" value = "${to}"></td>
						</tr>
						</table>

					<div class = "search_btn">
					<input id = "submit_btn" type="submit" value="検索する" >
					<input id = "submit_btn" type="button" onClick="javascript:resetTest();" id="clear" value="検索リセット"  />
					</div>

					</div>
			</form>
			</div>
		</div>
	</div>

	<div class = "overall">




		<div class = "errorMessages">
			<c:if test="${ not empty filterMessages}">
				<c:out value="${filterMessages}" />
				<c:remove var="filterMessages" scope="session"/>
			</c:if>
			<c:if test="${empty messages}">
				<c:out value = "検索条件に該当する投稿が見つかりませんでした。"/>
			</c:if>
		</div>
		<br/>

		<%--投稿表示--%>
		<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">

				<div class="title"><h1><c:out value="${message.title}" /></h1>
				</div>
				<div class="date">投稿日:<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<div class="text">
				<c:forEach  var = "text" items = "${fn:split(message.text, '
				')}"><c:out value = "${text}"/><br/>
				</c:forEach>

				</div>
			</div>
			<div class = "message-footer">
					<div class="category">カテゴリー:<c:out value="${message.category}" />
					</div>

					<div class="account-name">
						<span class="name">投稿者:<c:out value="${message.userName}" />
						</span>
					</div>
				</div>


			<c:if test = "${loginUser.id == message.userId }">

						<form action="messageDelete" method="post" >
						<div class = "massage-delete">
							<input type="hidden" value="${message.id}" name = "message_id">
							<input id = "reset_btn" type="submit" onClick = "return disp1();" value="投稿削除" >
						</div>
						</form>

			</c:if>



			<br />

			<%--コメント表示 --%>
			<div class="comment">
				<c:forEach items="${comments}" var="comment">
					<c:if test = "${message.id == comment.messageId }">

						<div class = "comment-area">
							<div class="comment-text">
								<c:forEach  var = "text" items = "${fn:split(comment.text, '
								')}"><c:out value = "${text}"/><br/>
								</c:forEach>
							</div>
							<div class = "comment-footer">
								<div class="comment-date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<div class="comment-account-name">
									<span class="name"><c:out value="${comment.userName}" />さんからのコメント
									</span>
								</div>
							</div>
						</div>

						<c:if test = "${loginUser.id == comment.userId }">

							<form action = "commentDelete" method="post" >
								<div class = "comment-delete">
									<input type = "hidden" name = "comment_id" value="${comment.id}">
									<input id = "reset_btn" type = "submit" onClick = "return disp2();" value="コメント削除" >
								</div>
							</form>
						</c:if>
					</c:if>
				</c:forEach>
			</div>
			<%--エラーメッセージ --%>
			<div class = "erorrMessage" id="commen-area-${message.id}">
				<c:if test="${not empty MessageID && MessageID == message.id}">
					<c:if test="${ not empty errorMessages }">


						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					<c:remove var="errorMessages" scope="session"/>

					</c:if>
				</c:if>
			</div>

			<%--コメント入力欄 --%>

			<form action="comment" method="post" >
			<div class="form-area" >
				<h2>コメント投稿（500文字以下)</h2>
				<input type="hidden" value="${message.id}" name = "message-id">

				<c:if test="${not empty MessageID && MessageID == message.id }">
					<textarea name = "new-comment" cols="100" rows="5" class="comment-box">${inputComment.text}</textarea>
					<br />
				</c:if>

					<c:if test="${MessageID != message.id }">

						<textarea name = "new-comment" cols="100" rows="5" class="comment-box" ></textarea>
						<br />

					</c:if>

				<input id = "submit_btn" type="submit" value="コメントする" >
			</div>

			</form>
		</c:forEach>
		<c:remove var= "comment" scope="session"/>
		<c:remove var= "MessageID" scope="session"/>
		</div>

		<br />


		<div class = "copyright">Copyright(c)Shimizu Risa</div>
	</div>

	</body>
</html>
