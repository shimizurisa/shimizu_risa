<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/message.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div class = "header-fixed">
			<p class = "menu"><a href="./" class = "link_btn"><span>ホーム</span></a></p>

			<p class ="main-title">新規投稿</p>
			<p class = "logout"><a href = "logout"class = "link_btn"><span>ログアウトする</span></a></p>

	</div>



		<div class="main-contents">


			<div class = "form">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
 								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
 					<c:remove var="errorMessages" scope="session"/>
				</c:if>

				<form action="newMessage" method="post">
				<div class="form-area">

					<table>
						<tr>
							<td id="test"><!-- <p class = "a"> --><label for = "title">タイトル(30文字以下)</label></td>
							<td id="hoge"><input name = "title" id = "title" value = "${Message.title }"></td>
						</tr>
						<tr>
							<td id="test"><!-- <p class = "c"> --><label for = "category">カテゴリー(10文字以下)</label></td>
							<td id="hoge"><input name = "category" id = "category" value = "${Message.category}"></td>
						</tr>

						<tr>
							<td colspan="2"><!-- <p class ="b"> --><label for = "message">本文(1000文字以下)</label><br/><!-- </p> -->
							<textarea name="message" cols="100" rows="10" class="message-box" ><c:out value = "${Message.text}"/></textarea></td>
						</tr>
					</table>




					<div class = "form-area2">
						<input id = "submit_btn" type="submit" value="投稿">
					</div>

					</div>
				</form>

			</div>
		</div>


		<div class = "copyright">Copyright(c)Shimizu Risa</div>

	</body>

</html>