<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	<link href="./css/signup.css" rel="stylesheet" type="text/css">
	</head>
		<body>
		<div class = "header-fixed">
			<p class = "menu"><a href="management"  class = "link_btn"><span>ユーザー管理</span></a>

			<p class ="main-title">新規ユーザー登録</p>
			<p class = "logout"><a href = "logout"class = "link_btn"><span>ログアウトする</span></a></p>

		</div>



		<div class="main-contents">
		<div class = "form">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>


			<form action="signup" method="post">
			<table>
				<tr>
					<td><label for="account">ログインID<br/>(半角英数字6～20文字)</label></td>
					<td><input name="account" id="account" value = "${signupUser.account }"/></td>
				</tr>
				<tr>
					<td><label for="password">パスワード<br/>(記号を含む半角英数字6～20文字)</label></td>
					<td><input name="password" type="password" id="password" /></td>
				</tr>
				<tr>
					<td><label for="password-confirm">パスワード(確認用)</label></td>
					<td><input name="password-confirm" type="password" id="password-confirm" /></td>
				</tr>
				<tr>
					<td><label for="name">名前(10文字以下)</label></td>
					<td><input name="name" id="name" value = "${signupUser.name}"/></td>
				<tr>
					<td><label for="branch_name">支店</label></td>

					<%-- <c:if test = "${editUser.id == loginUser.id}">
						<c:out value = "本社"/>
						<br/>
					</c:if> --%>

					<td><select name = "branch_id" id = "branch_id" >
						<c:forEach items = "${branches}" var = "branch">
								<c:if test = "${signupUser.branch_id == branch.id }">
								<option value = "${branch.id}" selected>${branch.name}</option>
							</c:if>
							<c:if test = "${signupUser.branch_id != branch.id }">
								<option value = "${branch.id}">${branch.name}</option>
							</c:if>
						</c:forEach>
					</select></td>
				<tr>
					<td><label for="department_name">部署・役職</label></td>
					<td>
					<select name = "department_id" id = "department_id" >
						<c:forEach items = "${departments}" var = "department">

							<c:if test = "${signupUser.department_id == department.id }">
								<option value = "${department.id}" selected>${department.name}</option>
							</c:if>
							<c:if test = "${signupUser.department_id != department.id }">
								<option value = "${department.id}">${department.name}</option>
							</c:if>

						</c:forEach>
					</select></td>
				</tr>
				</table>

			<input id = "submit_btn" type="submit" value="登録" /> <br />
			</form>
			</div>
		</div>
		<div class="copyright">Copyright(c)Shimizu Risa</div>
	</body>
</html>